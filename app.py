"""
Simple flask app that implements the UI for EMR demo
"""
import os
from contextlib import closing
from flask import Flask, request, session, g, redirect, \
     url_for, abort, render_template, flash
from collections import namedtuple
import redis
from emr import spark_job
import MySQLdb

# Constants
DB_DESTINATION_DIR = 'tmp'

# Initialize webapp
app = Flask(__name__)

# Load default config and override config from an environment variable
app.config.update(dict(
    DEBUG=True,
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))

# Init tools for flask request handling
@app.before_request
def before_request():
    if not hasattr(g, 'emrDB'):
        # EMR DB handling
        g.emrDB = MySQLdb.connect(host='52.24.251.40', port=3306,
                                  user='emr', passwd='abc123', db='emr')
    if not hasattr(g, 'R'):
        # REDIS DB handling
        g.R = redis.StrictRedis(host='localhost', port=6379, db=0)

@app.teardown_request
def teardown_request(exception):
    pass

# app.config.from_envvar('EMR_IR_SETTINGS', silent=True)


# DEFINE ENDPOINTS

Patient = namedtuple('Patient', ('person_id', 'gender', 'birthdate', 'uuid'))

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/api/emr')
def all_emr():
    """
    Gets a list of all patients in the database
    """
    cur = g.emrDB.cursor()
    cur.execute('SELECT * FROM person')
    return map(lambda p: Patient(p[0], p[1], p[2], p[15])._asdict(), cur.fetchall())

@app.route('/api/patient/<patient_id>')
def emr(patient_id):
    """
    Returns the details of a specific patient
    """
    cur = g.emrDB.cursor()
    cur.execute('SELECT given_name, family_name FROM person_name WHERE person_id = %s' % patient_id)
    first, last = cur.fetchone()
    cur.execute('SELECT birthdate FROM person where person_id = %s' % patient_id)
    birthdate = cur.fetchone()
    return {'patient_id': patient_id, 'first_name': first_name, 'last_name': last_name, 'birthdate': birthdate}

@app.route('/api/jobs', methods=['GET', 'POST'])
def jobs():
    """
    A 'GET' will return all the keys for Spark jobs
    """
    if request.method == 'GET':
        return g.R.keys('.*')
    else:
        return spark_job(request.form['query'], request.form['patient_id'])

@app.route('/api/job/<job_id>')
def job(job_id):
    return g.R.get(job_id)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

