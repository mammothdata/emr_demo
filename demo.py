"""
Spark Generated Patient Narratives (Rational EBM-NLP Fuzz Testing):
===================================================================
Working from real patient case studies (in this case, from the University of Pittsburgh Medical
Center), hand process the doctor's notes to form the templates. The templates are processed to
accept ICD-10 coded details with confirmation or negation stopwords and age, gender, race, size,
quantity, and duration modifiers. Performing an adhoc join on the ICD-10 disease/disorder code
with the EMR-bot patient corpus, laboratory and diagnostic test data is selected to match
the suspected template axes using a multinomial Bayes classifier.

In a final version, you could automatically scrape and process published patient case studies
and turn them into EBM (Evidence Based Medicine) diagnosable patient histories. These narratives
form a suitable corpora for machine learning in medical informatics without risking violation
of HIPPA.

Spark Powered Disease Classification:
=====================================
Create a multinomial naive bayes classifier, where the classifications match the ICD-10 disease and
disorder axes. The bayes classifier is trained by using the lab diagnostics from the EMR-bot lab and
diagnostics corpus.

All Together:
=============
The generated patient narratives will be very realistic and include enough data for doing NLP driven
statistical modeling of evidence based medicine.

For The Future:
===============
Can normalize the probability distributions based on the mortality statistics from the CDC. Then we can
build realistic general and target populations which EBM based simulations can be performed.

DEMO:
=====
First Demo -- show the generation of Fuzzy Data
"""
LOCAL = True

# import os
# os.putenv("SPARK_HOME", "/usr/local/Cellar/apache-spark/1.4.0/")
import logging
import re

import string
from string import Template
import json 
import random

from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer

from pyspark import SparkContext
from pyspark.mllib.feature import HashingTF
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.classification import NaiveBayes

# Module-level global variables for the `tokenize` function below
PUNCTUATION = set(string.punctuation)
STOPWORDS = set(stopwords.words('english'))
STEMMER = PorterStemmer()

# Template modifiers
MODIFIERS = dict(healthy_or_unhealthy=['healthy', 'unhealthy'],
                 age=map(str, range(10, 99)),
                 gender=['male', 'female'],
                 number=map(str, range(1, 11)),
                 duration=['weeks', 'months'],
                 small_or_copious=['small', 'copious'],
                 right_or_left=['right', 'left'],
                 no_or_some=['no', 'some'],
                 size=['large', 'small'],
                 race=['white', 'black', 'hispanic', 'native american', 'asian'],
                 increased_or_decreased=['increased', 'decreased'],
                 color=['red', 'green', 'blue', 'black', 'brown', 'light', 'dark'],
                 with_or_without=['with', 'without'],
                 condition_variability=['frequently', 'rarely'],
                 condition_polarity=['better', 'worse'],
                 frequency_modifier=['hourly', 'daily', 'weekly', 'monthly'],
                 confirmed_or_denied=['confirmed', 'denied'],
                 physiological_system_or_neurological=['neurological'],
                 normal_or_abnormal=['normal', 'abnormal'],
                 gender_pronoun=['he', 'she'],
                 possesive_gender_pronoun=['his', 'hers'],
                 upper_or_lower=['upper', 'lower'],
                 significant_or_insignificant=['significant', 'insignificant'],
                 vitals=['N/A'],
                 had_or_had_not=['had', 'had not'],
                 workup=['N/A'],
                 drug=['N/A'],
                 disorder=['N/A'],
                 mental_state=['N/A'],
                 smooth_or_rough=['smooth', 'rough'],
                 recent_or_distant=['recent', 'distant'],
                 remarkable_or_unremarkable=['remarkable', 'unremarkable'],
                 associated_or_not_associated=['associated', 'not associated'],
                 thickened_or_thinned=['thickened', 'thinned'],
                 active_or_inactive=['active', 'inactive'],
                 acute_or_chronic=['acute', 'chronic'],
                 was_or_was_not=['was', 'was not']
                )

# Intros #

INTRO = dict()

INTRO[0] = \
"""
A previously $healthy_or_unhealthy $age year old $race $gender patient presented 
to the emergency room.
"""

INTRO[1] = \
"""
The patient is a $age year old $gender with an unknown past medical history.
"""

INTRO[2] = \
"""
The patient is a $age year old $gender with no past medical history other than resolution 
of a recent $disorder.
"""

INTRO[3] = \
"""
A $age year old $gender with high fever (105 degrees F) for approximately $number $duration, 
accompanied by nausea, vomiting, arthralgias, and myalgias.
"""

INTRO[4] = \
"""
A $age year old $gender with a history of atrial fibrillation, hypertension, hyperlipidemia,
and gastroesophageal reflux presented to the emergency room with complaints of $number
days of $small_or_copious amounts of watery diarrhea.
"""

INTRO[5] = \
"""
This $age year old $gender presented to the emergency room with a history of $size
quarter sized midline tongue lesions present since childhood, which had recently 
$increased_or_decreased in size and was associated with spontaneous drainage of a dark $color
fluid. There was $no_or_some mucosal ulceration, or history of dyspnea, odynophagia or bleeding.
"""

INTRO[6] = \
"""
An $age year old $gender presented to the emergency room with a $number $duration 
history of $right_or_left $upper_or_lower abdominal pain accompanied by nausea with $no_or_some 
hematemsis, generalized malaise, cephalgia $with_or_without provocation, fever, and diarrhea.
"""

# Narratives #

NAR = dict()

NAR[0] = \
"""
It was $condition_variability and $condition_polarity with $frequency_modifier activity. The patient 
$confirmed_or_denied any prior history of $disorder. On physical examination, $no_or_some 
$physiological_system_or_neurological findings were noted.
"""

NAR[1] = \
"""
The police initially found $gender_pronoun $mental_state and acting violently. $gender_pronoun
was taken to the emergency room, where his initial vitals and physical exam were $significant_or_insignificant 
for $vitals; with mydriasis, dried blood in $possesive_gender_pronoun nares, skin pop marks on 
$possesive_gender_pronoun arms, and no focal.
"""

NAR[2] = \
"""
$gender_pronoun presented with abdominal pain after a meal that quickly worsened 
over the next 24 hours. Initial workup for the pain included $workup all of which were within 
$normal_or_abnormal ranges. A CT and chest x-ray were $normal_or_abnormal.
"""

NAR[3] = \
"""
$gender_pronoun reports that $gender_pronoun spent the past 15 months serving with the military in 
Afghanistan and only recently returned. $gender_pronoun has been taking mefloquine for malarial 
prophylaxis but discontinued it several $duration ago.
"""

NAR[4] = \
"""
$gender_pronoun admitted to anorexia but denied abdominal pain or cramping, nausea, fever, and blood 
or mucus in the stool. $gender_pronoun $had_or_had_not recently taken antibiotics and 
$possesive_gender_pronoun maintenance medications included $drug. The patient had a 
$recent_or_distant travel history including $duration in San Francisco, a weekened visiting 
family and a grandchild in daycare, and a $duration of boating in the Chesapeake Bay. During the 
boat trip the patient and other travelers consumed raw oysters. Approximately 24 hours after returning 
from the trip, the patient developed symptoms. Of the patient's traveling companions, some but not 
all reported similar ailments.
"""

NAR[5] = \
"""
On examination a 3 by 4 centimeter deflated, mobile cyst was observed in the tongue with $no_or_some
associated lymphadenopathy. A CT scan showed a midline soft tissue lesion in the tongue with a 
$normally_or_abnormally placed thyroid. $gender_pronoun was placed on augementin and scheduled 
for surgery. The specimen consisted of an oval excision of the dorsal tongue. The mucosa was grossly 
$remarkable_or_unremarkable. A 2 anterior posterior by 1 right left by 1 superficial deep centimeter 
$smooth_or_rough lined, unilocular cyst with variable wall thickness ranging from 2 tenths to 5 tenths 
of a centimeter was identified. No cyst contents were identified.
"""

NAR[6] = \
"""
Routine blood work showed $significant_or_insignificant elevations in AST, ALT, and lipase; 
leukopenia with an absolute lymphocytosis $associated_or_not_associated with reactive atypical 
lymphocytes seen by a manual differential; hypokalemia; and $normal_or_abnormal red cell indicies. 
Abdominal and chest CT as well as plain film chest roentgenograph were $remarkable_or_unremarkable. 
Abdominal ultrasound demonstrated a mildly $thickened_or_thinned gallbladder wall with $no_or_some 
pericholecystic fluid; $no_or_some calculi were seen. Nuclear hepatobiliary scan demonstrated 
patent cystic and common bile ducts. A liver biopsy, cholecystectomy, and appendectomy were performed. 
The liver biopsy demonstrated a chronic $active_or_inactive viral hepatitus. The gallbladder 
demonstrated $acute_or_chronic inflamation, was devoid of stones, and a diagnosis of acalulous 
cholecystitus $was_or_was_not rendered. The appendix demonstrated $remarkable_or_unremarkable 
epithelial lymphocytosis.
"""

def generate_modifiers(choice=None):
    """
    You can pass in custom "choosers" to customize to a desired probability distribution. This
    would be an excellent place to do markov chain monte carlo using spark. :)
    """
    choice = choice or random.choice
    # build dictionary of modifiers
    modifiers = dict()
    for key, value in MODIFIERS.iteritems():
        if key == 'gender':
            modifiers.setdefault(key, 'gender')
            # fix pronoun
            if 'gender' == 'male':
                modifiers.setdefault('gender_pronoun', 'he')
            else:
                modifiers.setdefault('possesive_gender_pronoun', 'she')
            # fix possessive
            if 'gender' == 'male':
                modifiers.setdefault('possesive_gender_pronoun', 'his')
            else:
                modifiers.setdefault('possesive_gender_pronoun', 'hers')
        # fuzz the data
        modifiers[key] = choice(value)
    return modifiers

def interpolate(template, choice=None):
    T = Template(template)
    return T.safe_substitute(generate_modifiers(choice=choice))


def main():
    sc = SparkContext()
    introductions = sc.parallelize(INTRO.values())
    narratives = sc.parallelize(NAR.values())
    pre_corpus = introductions.cartesian(narratives)
    corpus = pre_corpus.map(lambda x: interpolate(" ".join(map(str.strip, x))))

    with open('output.txt', 'w') as f:
        for patient_history in corpus.collect():
            f.write(patient_history)
            f.write('\n\n')

if __name__ == '__main__':
    main()