import re

import string
import json 

from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer

from pyspark import SparkContext
from pyspark.mllib.feature import HashingTF
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.classification import NaiveBayes

# Module-level global variables for the `tokenize` function below
PUNCTUATION = set(string.punctuation)
STOPWORDS = set(stopwords.words('english'))
STEMMER = PorterStemmer()

# Function to break text into "tokens", lowercase them, remove punctuation and stopwords, and stem them
def tokenize(text):
    tokens = word_tokenize(text)
    lowercased = [t.lower() for t in tokens]
    no_punctuation = []
    for word in lowercased:
        punct_removed = ''.join([letter for letter in word if not letter in PUNCTUATION])
        no_punctuation.append(punct_removed)
    no_stopwords = [w for w in no_punctuation if not w in STOPWORDS]
    stemmed = [STEMMER.stem(w) for w in no_stopwords]
    return [w for w in stemmed if w]

def main():
    # Initialize a SparkContext
    sc = SparkContext()

    # Import full dataset of newsgroup posts as text file
    data_raw = sc.textFile('s3://openmrs_concept_corpus')

    # Parse JSON entries in dataset
    data = data_raw.map(lambda line: json.loads(line))

    # Extract relevant fields in dataset -- category label and text content
    data_pared = data.map(lambda line: (line['label'], line['text']))

    # Prepare text for analysis using our tokenize function to clean it up
    data_cleaned = data_pared.map(lambda (label, text): (label, tokenize(text)))

    # Hashing term frequency vectorizer with 50k features
    htf = HashingTF(50000)

    # Create an RDD of LabeledPoints using category labels as labels and tokenized, hashed text as feature vectors
    data_hashed = data_cleaned.map(lambda (label, text): LabeledPoint(label, htf.transform(text)))

    # Ask Spark to persist the RDD so it won't have to be re-created later
    data_hashed.persist()

    # Split data 70/30 into training and test data sets
    train_hashed, test_hashed = data_hashed.randomSplit([0.7, 0.3])

    # Train a Naive Bayes model on the training data
    model = NaiveBayes.train(train_hashed)

    # Compare predicted labels to actual labels
    prediction_and_labels = test_hashed.map(lambda point: (model.predict(point.features), point.label))


# Find conclusion part of abstract and pass to tagger

def split_conclusion(sentence):
     int_conc_pos = 0
     if sentence.find("CONCLUSION") != -1:
          int_conc_pos = sentence.find("CONCLUSION") 
     elif sentence.find("Conclusion") != -1:
          int_conc_pos = sentence.find("conclusion") 
     elif sentence.find("INTERPRETATION") != -1:
          int_conc_pos = sentence.find("INTERPRETATION")
     elif sentence.find("DISCUSSION") != -1:
          int_conc_pos = sentence.find("DISCUSSION")
     else:
          int_conc_pos = 0        
     return  sentence[int_conc_pos:]
    
def sort_rules(rules_list):
    """
    Return sorted list of rules.
    
    Rules should be in a tab-delimited format: 'rule\t\t[four letter negation tag]'
    Sorts list of rules descending based on length of the rule, 
    splits each rule into components, converts pattern to regular expression,
    and appends it to the end of the rule.
    """
    rules_list.sort(key=len, reverse=True)
    sorted_list = []
    for rule in rules_list:
        s = rule.strip().split('\t')
        splitTrig = s[0].split()
        trig = r'\s+'.join(splitTrig)
        pattern = r'\b(' + trig + r')\b'
        s.append(re.compile(pattern, re.IGNORECASE))
        sorted_list.append(s)
    return sorted_list

class NegTagger(object):
    '''Take a sentence and tag negation terms and negated phrases.
    
    Keyword arguments:
    sentence -- string to be tagged
    phrases  -- list of phrases to check for negation
    rules    -- list of negation trigger terms from the sortRules function
    '''
    def __init__(self, sentence='', phrases=None, rules=None, negP=True):
        self.sentence = sentence
        self.phrases = phrases
        self.rules = rules
        self.neg_tagged_sentence = ''
        self.scopes_to_return = []
        self.negation_flag = None
        self.conclusion = ''
        
        filler = '_'
        
        # get the negation phrase and add underscore as filler
        for rule in self.rules:
            reformat_rule = re.sub(r'\s+', filler, rule[0].strip())
            self.sentence = rule[3].sub (' ' + rule[2].strip() = reformat_rule + rule[2].strip() + ' ', self.sentence)

        for phrase in self.phrases:
            phrase = splitConclusion(phrase) #get conclusion span
            self.conclusion = phrase # store conclusion to pass back to shell
            phrase = re.sub('([.^$*+?{\\|()[\]])', r'\\\1', phrase)
            joiner = r'\W+'
            joined_pattern = r'\b' + joiner.join(phrase) +  r'\b'
            reP = re.compile(joined_pattern, re.IGNORECASE)
            m = reP.search(self.sentence)
            if m:
                self.sentence = self.sentence.replace(m.group(0), '[PHRASE]'
                                                        + re.sub(r'\s+', filler, m.group(0).strip())
                                                        + '[PHRASE]')
                self.conclusion= self.sentence # store conclusion to pass back to shell
                
        neg_flag = 0
        #split on sentence tokens
        sentence_tokens = self.sentence
        sentence_tokens = split_conclusion(sentence_tokens) #get conclusion span
        #print sentence_tokens
        sentence_tokens = sentence_tokens.split()
        sentence_portion = ''
        a_scopes = []
        sb = []

        #check for negative phrases [NEGX]
        for i in range(len(sentence_tokens)):
            if sentence_tokens[i][:6] == '[NEGX]':
                neg_flag = 1
            
            if neg_flag == 1:
                sentence_tokens[i] = sentence_tokens[i].replace('[NEGX]', '[NEGATIVE]')
                sentence_portion = sentence_portion + ' ' + sentence_tokens[i]
            
            sb.insert(0, sentence_tokens[i])
        #print sentence_tokens
        if sentence_portion.strip():
            a_scopes.append(sentence_portion.strip())
            sb.reverse()
            self.neg_tagged_sentence = ' '.join(sb)

        if neg_flag == 1:
            self.negation_flag = 'Rejected'
        else:
            self.negation_flag = 'Accepted'

        self.neg_tagged_sentence = self.neg_tagged_sentence.replace(filler, ' ')

        for line in a_scopes:
            tokens_to_return = []
            this_line_tokens = line.split()
            self.scopes_to_return.append(' '.join(tokens_to_return))

    def __str__(self):
        text = self._neg_tagged_sentence
        text += '\t' + self.conclusion
        text += '\t' + self.negation_flag
        text += '\t' + '\t'.join(self.scopes_to_return)


# """
# To be used inside an iPython notebook
# """
# from textblob import TextBlob
# from pyspark import SparkContext
# from pyspark.mllib.feature import HashingTF
# from pyspark.mllib.regression import LabeledPoint
# from pyspark.mllib.classification import NaiveBayes
# from itertools import chain
# import sys
# import uuid
# from collections import namedtuple
# import MySQLdb

# # Wrappers for OpenMRS tables of interest
# Concept = namedtuple('Concept', ' '.join(('concept_id', 'retired', 'short_name', 'description', 'form_text', 'datatype_id', 'class_id', 'is_set', 'creator', 'date_created', 'version', 'changed_by', 'date_changed', 'retired_by',' date_retired', 'retire_reason', 'uuid')))
# ConceptDescription = namedtuple('ConceptDescription', ' '.join(('concept_description_id', 'concept_id', 'description', 'locale', 'creator', 'date_created', 'changed_by', 'date_changed', 'uuid')))
# ConceptName = namedtuple('ConceptName', ' '.join(('concept_id', 'name', 'locale', 'creator', 'date_created', 'concept_name_id', 'voided', 'voided_by', 'date_voided', 'void_reason', 'uuid', 'concept_name_type', 'locale_preferred')))
# Observation = namedtuple('Observation',' '.join(('obs_id', 'person_id', 'concept_id', 'encounter_id', 'order_id', 'obs_datetime', 'location_id', 'obs_group_id', 'accession_number', 'value_group_id', 'value_boolean', 'value_coded', 'value_coded_name_id', 'value_drug', 'value_datetime', 'value_numeric', 'value_modifier', 'value_text', 'comments', 'creator', 'date_created', 'voided', 'voided_by', 'date_voided', 'void_reason', 'value_complex', 'uuid', 'previous_version', 'bleh')))

# def extract_data():
#     myDB = MySQLdb.connect(host='52.24.251.40', port=3306, user='emr', passwd='abc123', db='emr')
#     cur = myDB.cursor()

#     def concepts():
#         cur.execute('SELECT * FROM concept')
#         concepts = map(lambda x: Concept(*x), cur.fetchall())
#         return concepts

#     def descriptions():
#         cur.execute('SELECT * FROM concept_description')
#         concept_descriptions = map(lambda x: ConceptDescription(*x), cur.fetchall())
#         return concept_descriptions

#     def names():
#         cur.execute('SELECT * FROM concept_name')
#         concept_names = map(lambda x: ConceptName(*x), cur.fetchall())
#         return concept_names

#     def observations():
#         cur.execute('SELECT * FROM obs')
#         observations = map(lambda x: Observation(*x), cur.fetchall())
#         return observations

#     concepts_map = dict((x.concept_id, x) for x in concepts())
#     names_map = dict((x.concept_id, x) for x in names())
#     descr_map = dict((x.concept_id, x) for c in descriptions())
#     obs_map = dict((obs.concept_id, obs) for obs in observations)
#     return concepts_map, names_map, descr_map, obs_map

# def learn(concepts_map, names_map, descr_map, obs_map):
#     sc = SparkContext()
#     # Hashing term frequency vectorizer with 50k features
#     htf = HashingTF(50000)
#     # Create an RDD of LabeledPoints using category labels as labels and tokenized, hashed text as feature vectors
#     data_hashed = data_cleaned.map(lambda (label, text): LabeledPoint(label, htf.transform(text)))
#     # Ask Spark to persist the RDD so it won't have to be re-created later
#     data_hashed.persist()
#     # Split data 70/30 into training and test data sets
#     train_hashed, test_hashed = data_hashed.randomSplit([0.7, 0.3])
#     # Train a Naive Bayes model on the training data
#     model = NaiveBayes.train(train_hashed)
#     # Compare predicted labels to actual labels
#     prediction_and_labels = test_hashed.map(lambda point: (model.predict(point.features), point.label))


# def label_data():
#     cm, nm, dm, om = extract_data()
#     for c_id in cm:
#         LabeledPoint(c_id, nm[c_id], dm[c_id])

# def build_semantic_vector_space():
#     """
#     Forms the semantic vector space from which vector arithmetic may be used to
#     perform inference on EMR data.

#     Uses the raw OpenMRS EMR dump (small) and a slightly pruned raw dump of Wikipedia-EN
#     """
#     sc = SparkContext(appName='Word2Vec')
#     word2vec = Word2Vec()
#     emr_corpus = sc.textFile(EMR_DUMP).map(lambda row: row.split(" "))
#     wik_corpus = sc.textFile(WIK_DUMP).map(lambda row: row.split(" "))
#     sno_corpus = sc.textFile(SNO_DUMP).map(lambda row: row.split(" "))
#     icd_corpus = sc.textFile(ICD_DUMP).map(lambda row: row.split(" "))
#     model = word2vec.fit(chain(emr_corpus, wik_corpus, sno_corpus, icd_corpus))
#     return model

# def doc2vec(doc, model):
#     """
#     Takes a string (word, sentence, whole document) and uses the autosumarization
#     property of Word2Vec to find the nearest vector (by cosine similarity).
#     """
#     return model.transform(doc)

# def infer(question, model):
#     """
#     By using Wikipedia-EN as a supporting corpus, we have implicit contextual awareness.
#     This means we do not have to attach a query language and can ask questions in plain English.
#     """
#     return model.findSynonyms(doc2vec(question))

# def spark_job(query, emr_id):
#     """
#     Returns the differential diagnosis of a patient.
#     """
#     model = build_semantic_vector_space()
#     answer = infer(sample_query.format(emr_id, model)) + \
#              model.transform(query) - \
#              infer(emr_id, model)
#     return (uuid.uuid1(), answer)

if __name__ == '__main__':
    main(sys.argv[0])
