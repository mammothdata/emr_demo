"""
emr.py - Disease Inference Using Deep Learning and Spark

Parts:
* Medscape Spider
* Medscape Corpus Parser
* Spark Diagnostic Machinery

Medscape Spider
---------------
Uses Scrapy API to query and subsequently record the Medscape online infectious disease reference.

Medscape Corpus Parser
----------------------
Preps the serialized infectious disease reference, primes the machine learner, then performs Word2Vec
deep learning on the Medscape corpus.

Spark Diagnostic Machinery
--------------------------
First use GenSim to do Latent Semantic Analysis (LSA) or Latent Dirchelet Analysis (LDA) on the EMR data,
where each EMR is a document in the corpus and where the generated topics represent symptomology
to be introspected by Spark.

Once the topic analysis done, we are left with a constellation of symptoms in the form of multiple
Bag of Words (BoW), from which Spark can perform the Word2Vec similarity comparison between a particular
medical record and the parsed corpus of infectious disease.
"""

__version__   = "0.1"
__pyversion__ = ">=3.4"

from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.types import Row, StructField, StructType, StringType, IntegerType
from pyspark import SparkContext
from pyspark.mllib.feature import Word2Vec
# import scrapy
import json

# Define a spider for scraping and parsing the training corpus from Medscape's
# infectious disease reference.

# class MedscapeSpider(scrapy.Spider):
#     # bookeeping for scrapy
#     name = 'medscape_spider'
#     start_url = ['http://emedicine.medscape.com/infectious_diseases/articles']

#     # private helper methods

#     @staticmethod
#     def _parse_text(response):
#         try:
#             # grab the sentences
#             yield from iter(response.css('refsection_content').split('.'))
#         finally:
#             return # always close the generator when finished, clean micro-threading!

#     @staticmethod
#     def _parse_next(response, css_selector, next_callback):
#         for href in response.css(css_selector):
#             full_url = response.urljoin(href.extract() + ['#showall'])
#             yield scrapy.Request(full_url, callback=callback)

#     # begin parse-chain
#     def parse(self, response):
#         yield from self._parse_text(response)
#         yield from self._parse_next(response, '.maincolbox', self.parse_disease)

#     def parse_disease(self, response):
#         yield from self._parse_text(response)
#         yield from self._parse_next(response, '.next_btn1', self.parse_overview)

#     def parse_overview(self, response):
#         yield from self._parse_text(response)
#         yield from self._parse_next(response, '.next_btn1', self.parse_presentation)

#     def parse_presentation(self, response):
#         yield from self._parse_text(response)
#         yield from self._parse_next(response, '.next_btn1', self.parse_ddx)

#     def parse_ddx(self, response):
#         yield from self._parse_text(response)
#         yield from self._parse_next(response, '.next_btn1', self.parse_workup)

#     def parse_workup(self, response):
#         yield from self._parse_text(response)
#         yield from self._parse_next(response, '.next_btn1', self.parse_medication)

#     def parse_medication(self, response):
#         yield from self._parse_text(response)
#         yield from self._parse_next(response, '.next_btn1', self.parse_follow_up)

#     def parse_follow_up(self, response):
#         yield from self._parse_text(response)
#         yield from self._parse_next(response, '.next_btn1', self.parse_treatment)

#     def parse_treatment(self, response):
#         yield from self._parse_text(response)

#     def persist(self, content):
#         pass


# class MedscapeCorpusParser:
#     def __init__(self, filename_or_model):
#         self.model = self.load_medscape_corpus(filename) if \
#                         isinstance(filename_or_model, str) else self.learn(filename_or_model)

#     # Handle loading and parsing of corpus for deep learning
#     def load_medscape_corpus(self, filename):
#         return Word2Vec.load_word2vec_format(filename, binary=False)

#     # Perform deep learning
#     def learn(self, parsed):
#         from collections import Iterable
#         assert isinstance(parsed, Iterable)
#         return Word2Vec(parsed, size=100, window=5, min_count=5, workers=4)

#     def most_similar(self, positive=None, negative=None):
#         return self.model.most_similar(positive=positive, negative=negative)

#     # Convenience method
#     def __call__(self, positive=None, negative=None):
#         return self.most_similar(positive, negative)

WIKI_PATH = ''
DISEASE_INDEX = ''

def do_learn():
    pass

def infer_disease():
    pass

def main(emr):
    sc = SparkContext(appName='Word2Vec')
    inp = sc.textFile(WIKI_PATH).map(lambda row: row.split(" "))
    word2vec = Word2Vec()
    model = word2vec.fit(inp)
    with open(DISEASE_INDEX, 'r') as d_index:
        for disease in d_index:
            synonyms = model.findSynonyms(disease, 40)
            for word, cosine_distance in synonyms:
                print("{}: {}".format(word, cosine_distance))
        sc.stop()

if __name__ == '__main__':
    main()