"""
Simple flask app that implements the UI for EMR demo
"""
import os
from enum import Enum
import sqlite3
import requests
from requests.auth import HTTPBasicAuth
from contextlib import closing
from flask import Flask, request, session, g, redirect, \
     url_for, abort, render_template, flash

# Constants
DB_DESTINATION_DIR = 'tmp'

# Initialize webapp
app = Flask(__name__)

# Load default config and override config from an environment variable
app.config.update(dict(
    DEBUG=True,
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))

app.config.from_envvar('EMR_IR_SETTINGS', silent=True)

# Database handling
class DB(Enum):
    """
    Simplified DB handling. For instance, to get the EMR cursor,

    >>> emr_cursor = DB.EMR.connect_db()
    """
    EMR = 1
    WIK = 2
    RUN = 3
    APP = 4

    def connect_db(self, app=None):
        path = self.whereami(app)
        return sqlite3.connect(path)

    def whereami(self, app=None):
        """
        Facilitates path resolution for db handles by applying appropriate pre
        and postfixes. `IR` is prefixed to the lowercase name of the database
        as listed in the enum,

        >>> print(DB.EMR.whereami())
        '../tmp/IR_EMR.db'
        """
        name = 'IR_{0}.db'.format(self.name.lower())
        root = app.root_path if app else ''
        return os.path.join(root, DB_DESTINATION_DIR, name)

    def init_db(self, schema, app=None):
        with closing(self.connect_db(app)) as db:
            with app.open_resource(schema, mode='r') as f:
                db.cursor().executescript(f.read())
            db.commit()

# Init tools for flask request handling
# @app.before_request
# def before_request():
#     g.db = connect_db()

# @app.teardown_request
# def teardown_request(exception):
#     db = getattr(g, 'db', None)
#     if db is not None:
#         db.close()

EVENT_URL = 'https://bitbucket.org/mammothdata/emr_demo/events'

def get_events():
    creds = HTTPBasicAuth(app.config['USERNAME'], app.config['PASSWORD'])
    events = requests.get(EVENT_URL, auth=creds)
    if events.status_code != 200:
        return [{'description': 'N/A',
                 'event_id'   : 'N/A',
                 'created_on' : 'N/A'}]
    retval = []
    for e in events.json():
        retval.append({'description': e['description'],
                       'event_id'   : e['event'],
                       'created_on' : e['created_on']})
    return retval


@app.route('/')
def index():
    return render_template('index.html', events=get_events())

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if not session['logged_in']:
            app.config['USERNAME'] = request.form['username']
            app.config['PASSWORD'] = request.form['password']
            session['logged_in'] = True
            return redirect('/')
    return render_template('index.html', error=error)

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    return redirect('/')

DEFAULT_EMR = {"patient_id": None, "fist_name": None, "last_name": None
                "dob": None, "notes": None}

@app.route('/api/emr/<patient_id>', methods=['GET', 'PUT'])
def emr(patient_id):
    # db = DB.EMR.connect_db()
    if request.method == 'GET':
        return DEFAULT_EMR
    elif request.method == 'PUT':
        return DEFAULT_EMR

DEFAULT_JOB = {'job_id': None, 'is_done': False, 'time_started': None}
DEFAULT_JOBS = [DEFAULT_JOB]


@app.route('/api/jobs', methods=['GET', 'POST'])
def jobs():
    if request.method == 'GET':
        return DEFAULT_JOBS
    elif request.method == 'POST':
        return DEFAULT_JOB

@app.route('/api/job/<job_id>', methods=['GET'])
def job(job_id):
    return DEFAULT_JOB

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)