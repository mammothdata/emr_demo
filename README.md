**EMR Based Infection Recognition With Apache Spark**
=====================================================

The aim of this project is to demonstrate the *low barrier* to entry for Big Data in the medical space, specifically by developing a *lightweight* and *modular* platform for processing Electronic Medical Records (EMR) to indicate the presence of a number of different infectious diseases. Ultimately, this modularity will be exploited to generalize insights derived at the patient level and apply them to large scale public health informatics.

The corpora and the tech stack are freely available and open source:

1. Corpus - Open MRS and Medscape Infectious Disease Reference
1. Stack  - Apache Spark with GraphX (python bindings), Apache ML (word2vec), Python 3.4 (with various 3rd party libraries available from pypi.python.org)

**Spark** is a framework for event based stream processing over directed acyclic graphs (DAGs) and GraphX is a framework for building graph based computational models, easily parallelizable and amenable to high throughput, low cost distributed computing -- both will be leveraged to create modular system for recognizing the presence of specific infectious diseases. **Blurb about openmrs** ...

Implementation Notes:
=====================

* Medscape Spider
* Medscape Corpus Parser
* Spark Diagnostic Machinery

Medscape Spider
---------------

Uses Scrapy API to query and subsequently record the Medscape online infectious disease reference.

Medscape Corpus Parser
----------------------

Preps the serialized infectious disease reference, primes the machine learner, then performs Word2Vec learning on the Medscape corpus.

Spark Diagnostic Machinery
--------------------------

The scope of the spark processing depends on the scale at which we are trying to infer the presence or risk of infection. The architecture is independent of use case, as whether we operate on an individual, a sample population, or the population as a whole, the process remains essentially the same -- identify clusters of symptoms, vectorize via word2vec, and compare the result with disease entries in the pre-vectorized medscape reference.